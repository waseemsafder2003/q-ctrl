import React from 'react';
import ReactDOM from 'react-dom';
import App from '../src/app';
import { Provider } from 'react-redux';//We Using Provider to provide store to our app.
import configureStore from '../src/store';
import * as serviceWorker from './serviceWorker';
import "./index.css";
const store = configureStore();

ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
