import { all } from 'redux-saga/effects';
import controlsSagas from './Controls';

export default function* rootSaga(getState) {
    yield all([
        controlsSagas()
    ]);
}
