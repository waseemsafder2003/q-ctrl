import { all, call, fork, put, takeLatest } from "redux-saga/effects";

import {
    GET_CONTROLS,
} from "../constants/ActionTypes";
import {API_BASE_URL} from "../constants/configs"
import { setContolsLoader, setControls } from "../actions/Controls";
//Fake data to be return as API is not accessible 
const data = [
    {
        type: "controls",
        id: "1",
        attributes: {
            name: "Single-Qubit Driven",
            type: "Primitive",
            maximum_rabi_rate: 63.16731,
            polar_angle: 0.05671
        }
    },
    {
        type: "controls",
        id: "2",
        attributes: {
            name: "Single-Qubit Dynamic Decoupling",
            type: "CORPSE",
            maximum_rabi_rate: 87.00172,
            polar_angle: 0.02688
        }
    },
    {
        type: "controls",
        id: "3",
        attributes: {
            name: "Two-Qubit Parametric Drive",
            type: "Gaussian",
            maximum_rabi_rate: 70.03844,
            polar_angle: 0.09843
        }
    },
    {
        type: "controls",
        id: "4",
        attributes: {
            name: "Mølmer-Sørensen Drive",
            type: "CinBB",
            maximum_rabi_rate: 97.07732,
            polar_angle: 0.09173
        }
    }
];
const fakeApiCall=(flag)=>{
    //if flag is true then we return array else we return error in set time out
    return new Promise((resolve,reject)=>{
        if(flag)
        {
            setTimeout(() => {
                      resolve(data)
            },1000)
        }
        else
        {
            setTimeout(() => {
                reject(new Error("SomeThing Went Wrong"))
      },1000)
        }
    })
}
//to call get api 
// const getControlRequest = async (url) => {
//     return await fetch(url, {
//         method: 'get',
//         headers: {
//             "Content-type": "application/json"
//         }
//     })
//         .then(reponse => reponse.json()).catch(e => e);
// }

function* getControlsSaga({ payload }) {
    try {
        yield put(setContolsLoader({ isFailed: false, message: "loading controls", loader: true }));
        let url = `${API_BASE_URL}/accounts`;//in case of no payload,we will change url to Null
        if(!payload)
        {
            url=null;
        }
        const controls = yield call(fakeApiCall, url);
        if (!controls.error) {
            yield put(setControls([...data]));
        } else {
            yield put(setContolsLoader({ isFailed: true, message: "Unable To Load Controls", loader: false }));
        }
    } catch (error) {
        yield put(setContolsLoader({ isFailed: true, message: "Unable To Load Controls", loader: false }));

    }
}



export function* requestGetControls() {
    yield takeLatest(GET_CONTROLS, getControlsSaga);
}

export default function* rootSaga() {
    yield all([
        fork(requestGetControls)]);
}