import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Fade from '@material-ui/core/Fade';
import CloseIcon from '@material-ui/icons/Close';
import PropTypes from "prop-types";
const useStyles = makeStyles(theme => ({
    modal: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    }
}));

export default function AlertModal(p) {
    const classes = useStyles();
    return (
        <div>
            <Modal
                className={classes.modal}
                open={p.open}
                onClose={p.handleCloseModal}
                closeAfterTransition
            >
                <Fade in={p.open}>
                    <div className="bg-white">
                        <CloseIcon onClick={p.handleCloseModal} fontSize="default" className="close-icon" />
                        <div className="modal-text">
                            <p id="title">{p.title}</p>
                            <h3 id="message" className="m-0">{p.message}</h3>
                            <button
                                className="btn btn-primary"
                                onClick={() => {
                                    p.getControls(1)
                                    p.handleCloseModal()
                                }}>TRY AGAIN</button>
                        </div>
                    </div>
                </Fade>
            </Modal>
        </div>
    );
}
AlertModal.propTypes={
    handleCloseModal: PropTypes.func.isRequired,
    open : PropTypes.bool.isRequired,
    getControls : PropTypes.func.isRequired,
    title : PropTypes.string.isRequired,
    message : PropTypes.string.isRequired
}
