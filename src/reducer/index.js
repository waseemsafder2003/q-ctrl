import { combineReducers } from 'redux';
import Controls from './Controls';


export default () => combineReducers({
  controls: Controls,
});
