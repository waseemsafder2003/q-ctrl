import {
    SET_CONTROLS_LOADER, SET_CONTROLS
} from "../constants/ActionTypes";

const INIT_STATE = {
    isFailed: false,
    message: "",
    loader : false,
    controlsArr: [],

};


export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case SET_CONTROLS: {
            return {
                ...state,
                loader: false,
                message: "",
                controlsArr: action.payload
            }
        }
        case SET_CONTROLS_LOADER: {
            const { loader, isFailed, message } = action.payload;
            return {
                ...state,
                loader,
                isFailed,
                message
            }
        }
        default:
            return state;
    }
}
