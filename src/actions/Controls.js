import {
    GET_CONTROLS, SET_CONTROLS, SET_CONTROLS_LOADER
} from '../constants/ActionTypes';

export const getControls = (payload) => {
    return {
        type: GET_CONTROLS,
        payload
    };
};
export const setControls = (payload) => {
    return {
        type: SET_CONTROLS,
        payload
    };
};
export const setContolsLoader = (payload) => {
    return {
        type: SET_CONTROLS_LOADER,
        payload
    };
};


