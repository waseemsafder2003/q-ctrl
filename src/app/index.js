import React from 'react';
import { connect } from "react-redux";
import { getControls } from "../actions/Controls";
import Nav from "../components/nav";
// import "./style.css";
//Plus icon
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
//table
import DataTable from "./table/index";

import CircularProgress from '@material-ui/core/CircularProgress';
import AlertModal from "../components/modal/Alert";

class App extends React.Component {
    constructor() {
        super();
        this.state = {
            showModal: false
        }
    }

    componentDidMount() {
        //in component did mount we calling getControls with no arguments
        //wheres in modal when we press "try again" button then we pass "1" as argumemt
        this.props.getControls();
    }
    componentDidUpdate(prevP, prevS) {
        if (prevP !== this.props) {
            //object destructring
            const { loader, isFailed } = this.props;
            //if loader stoped
            if (prevP.loader && !loader) {
                //if loading failed then we will show modal of error otherwise modal will be false
                if (isFailed) {
                    this.setState({ showModal: true })
                }
                else {
                    this.setState({ showModal: false })
                }
            }
        }
    }
    handleCloseModal = () => {
        this.setState({ showModal: false })
    }
    render() {
        const { loader, controlsArr = [], getControls, message } = this.props;
        const { showModal } = this.state;
        return (
            <div className="main">
                {/* nav bar */}
                <Nav />
                {/* when loading failed then we have to display modal
                and we are setting showModal true in componentDidUpdate */}
                {showModal && <AlertModal
                    handleCloseModal={() => this.handleCloseModal()}//it will be called on close modal
                    open={showModal}
                    getControls={getControls}//get control function called on try again in modal 
                    title={"Error"}//title of error modal
                    message={message} /> //error message
                }
                <div className="center">
                {
                /* Initail value of the loader is true because we are calling apis in saga
                and when loader is true, we are displaying the loader component "CircularProgress" */}
                    {loader ?

                        <CircularProgress size={"10rem"} style={{ marginTop: "5rem" }} />

                        : <div className="inner">

                            <div className="add-icon" >

                                <h4 style={{ marginLeft: "1rem", marginRight: "1rem" }}>Controls </h4>

                                <Fab size="small" color="primary" > 
                                    <AddIcon />

                                </Fab>

                            </div>

                            {/* table on feched data. we are passing controls array as data to DataTable */}
                            <DataTable data={controlsArr} />

                        </div>
                    }

                </div>

            </div>

        );
    }
}
const mapStateToProps = ({ controls }) => {
    const { loader, isFailed, message, controlsArr } = controls;
    return { loader, isFailed, message, controlsArr };
}
export default connect(mapStateToProps, {
    getControls
})(App);
//connect function takes two arguments first matStateToProps and second mapDispactToProps
