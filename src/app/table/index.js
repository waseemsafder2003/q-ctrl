import React from "react";
import PropTypes from 'prop-types';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { withStyles, } from '@material-ui/core/styles';
const StyledTableCell = withStyles(theme => ({
    head: {
        fontSize: 13,
    },
    body: {
        fontSize: 13,
        backgroundColor: theme.palette.common.white,
    },
}))(TableCell);
const DataTable = (p) => {
    return (
        <TableContainer style={{ marginTop: "1rem" }}>
            <Table>
                <TableHead >
                    <TableRow>
                        <StyledTableCell >TITLE</StyledTableCell>
                        <StyledTableCell align="left">TYPE&nbsp;</StyledTableCell>
                        <StyledTableCell align="left">POLAR ANGLE&nbsp;</StyledTableCell>
                        <StyledTableCell align="left">MAX RABI RATE&nbsp;</StyledTableCell>
                        <StyledTableCell align="left"> &nbsp;</StyledTableCell>
                        <StyledTableCell align="left"></StyledTableCell>

                    </TableRow>
                </TableHead>
                <TableBody>
                    {p.data.map(control => (
                        <TableRow key={control.id}>
                            <StyledTableCell scope="row">
                                {control.attributes.name}
                            </StyledTableCell>
                            <StyledTableCell align="left"><span className="badge"> {control.attributes.type}</span></StyledTableCell>
                            <StyledTableCell align="left">{control.attributes.polar_angle}</StyledTableCell>
                            <StyledTableCell align="left">{control.attributes.maximum_rabi_rate}</StyledTableCell>
                            <StyledTableCell align="left"> &nbsp;</StyledTableCell>
                            <StyledTableCell align="left"><b style={{ cursor: "pointer" }}>{">"}</b></StyledTableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}
DataTable.propTypes = {
    data: PropTypes.array.isRequired
};
export default DataTable;