import React from 'react';
import { render } from '@testing-library/react';
import App from './App';

test('QCTRL Test', () => {
  const { getByText } = render(<App />);
  const linkElement = getByText(/Q-CTRL TEST/i);
  expect(linkElement).toBeInTheDocument();
});
